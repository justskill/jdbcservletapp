package com.example.jdbcservletapp.servlet;

import com.example.jdbcservletapp.dao.UserDAO;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html");

        String id = request.getParameter("id");


        try {
            UserDAO.deleteById(Long.parseLong(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
