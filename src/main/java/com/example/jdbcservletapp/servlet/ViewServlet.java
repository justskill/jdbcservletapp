package com.example.jdbcservletapp.servlet;

import com.example.jdbcservletapp.dao.UserDAO;
import com.example.jdbcservletapp.model.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();


        List<User> list = null;
        try {
            list = UserDAO.getUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        out.println("<h1>Users</h1>");

        out.print("<table border='1' width='100%'");
        out.print("<tr><th>Id</th><th>Firstname</th><th>Lastname</th><th>Age</th></tr>");

        for(User user:list){
            out.print("<tr><td>"+user.getId()+"</td><td>"+user.getFirstName()+"</td><td>"+user.getLastName()+"</td><td>"+user.getAge()+"</td></tr>");
        }

    }

    }

