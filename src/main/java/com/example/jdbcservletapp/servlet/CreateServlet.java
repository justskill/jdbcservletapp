package com.example.jdbcservletapp.servlet;


import com.example.jdbcservletapp.dao.UserDAO;
import com.example.jdbcservletapp.model.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        //String id = request.getParameter("id");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String age = request.getParameter("age");

        User user = new User();
        //user.setId(Long.parseLong(id));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAge(Integer.parseInt(age));


        try {
            UserDAO.createUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
