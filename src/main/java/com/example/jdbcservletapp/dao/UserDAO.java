package com.example.jdbcservletapp.dao;

import com.example.jdbcservletapp.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    Connection conn = null;

    public static Connection getConnection() throws SQLException {
        Connection conn = null;
        String url = "jdbc:postgresql://192.168.153.130:5432/users";
        String username = "postgres";
        String password = "mydbpass";
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        conn = DriverManager.getConnection(url,username,password);
        return conn;
    }

    public static int createUser(User user) throws SQLException {
        try (Connection conn = UserDAO.getConnection()){
            int status;
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO users (firstName, lastName, age) VALUES(?,?,?)");
            //preparedStatement.setLong(1, user.getId());
            preparedStatement.setString(1,user.getFirstName());
            preparedStatement.setString(2,user.getLastName());
            preparedStatement.setInt(3,user.getAge());

            return status = preparedStatement.executeUpdate();
        }
    }

    public static List<User> getUsers() throws SQLException {
        try (Connection conn = UserDAO.getConnection()){
            List<User> list = new ArrayList<>();

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM users");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                User user = new User();
                user.setId(rs.getLong(1));
                user.setFirstName(rs.getString(2));
                user.setLastName(rs.getString(3));
                user.setAge(rs.getInt(4));
                list.add(user);
            }

            return list;
        }
    }

    public static int updateUser(User user) throws SQLException {
        try (Connection conn = UserDAO.getConnection()) {
            int status;
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE users SET firstName=?,lastName=?,age=? where id=?");
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setInt(3, user.getAge());
            preparedStatement.setLong(4, user.getId());

            return status = preparedStatement.executeUpdate();
        }
    }

    public static int deleteById(long id) throws SQLException {
        try (Connection conn = UserDAO.getConnection()){
            int status;
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM users where  id=?");
            preparedStatement.setLong(1,id);

            return status = preparedStatement.executeUpdate();
        }
    }



}
